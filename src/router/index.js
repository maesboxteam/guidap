import Vue from 'vue'
import Router from 'vue-router'

import GuidAp from '@/components/GuidAp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: GuidAp
    }
  ]
})
